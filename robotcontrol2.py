#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'saf'


import sys
import serial
import threading
import argparse
import select
import logging

# Set Up Logging
logging.basicConfig(format='%(asctime)-15s [%(threadName)-25s] %(message)s')

class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            try:
                self.impl = _GetchMacCarbon()
            except AttributeError:
                self.impl = _GetchUnix()

    def __call__(self): return self.impl()

class _GetchUnix:
    def __init__(self):
        import tty, sys, termios # import termios now or else you'll get the Unix version on the Mac

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()

class _GetchMacCarbon:
    """
    A function which returns the current ASCII key that is down;
    if no ASCII key is down, the null string is returned.  The
    page http://www.mactech.com/macintosh-c/chap02-1.html was
    very helpful in figuring out how to do this.
    """
    def __init__(self):
        import Carbon
        Carbon.Evt #see if it has this (in Unix, it doesn't)

    def __call__(self):
        import Carbon
        if Carbon.Evt.EventAvail(0x0008)[0]==0: # 0x0008 is the keyDownMask
            return ''
        else:
            #
            # The event contains the following info:
            # (what,msg,when,where,mod)=Carbon.Evt.GetNextEvent(0x0008)[1]
            #
            # The message (msg) contains the ASCII char which is
            # extracted with the 0x000000FF charCodeMask; this
            # number is converted to an ASCII character with chr() and
            # returned
            #
            (what,msg,when,where,mod)=Carbon.Evt.GetNextEvent(0x0008)[1]
            return chr(msg & 0x000000FF)

class InputThread(threading.Thread):

    def __init__(self, serial):
        threading.Thread.__init__(self)
        self.serial = serial
        self.name = 'keyboard_input_thread'

    def run(self):
        logger.debug("running")
        inkey = _Getch()
        while True:
            c = inkey()
            print "KEYBOARD: %s" % c
            if c == 'b':
                print "Will exit"
                sys.exit(0)
            elif c == 'q':  # links vorwärts
                cmd = "ML050R100|"
                cmd = "E" + chr(1) + chr(50) + chr(1) + chr(100)
            elif c == 'w':  # vorwärts
                cmd = "ML100R100|"
                cmd = "E" + chr(1) + chr(100) + chr(1) + chr(100)
            elif c == 'e':  # rechts vorwärts
                cmd = "ML100R050|"
                cmd = "E" + chr(1) + chr(100) + chr(1) + chr(50)
            elif c == 'a':  # links
                cmd = "Ml100R100|"
                cmd = "E" + chr(0) + chr(100) + chr(1) + chr(100)
            elif c == 's':  # stop
                cmd = "ML000R000|"
                cmd = "E" + chr(0) + chr(0) + chr(0) + chr(0)
            elif c == 'd':  # rechts
                cmd = "ML100r100|"
                cmd = "E" + chr(1) + chr(100) + chr(0) + chr(100)
            elif c == 'y':  # links rückwärts
                cmd = "Ml050r100|"
                cmd = "E" + chr(0) + chr(50) + chr(0) + chr(100)
            elif c == 'x':  #rückwärts
                cmd = "Ml100r100|"
                cmd = "E" + chr(0) + chr(100) + chr(0) + chr(100)
            elif c == 'c':  # rechts rückwärts
                cmd = "Ml100r050|"
                cmd = "E" + chr(0) + chr(100) + chr(0) + chr(50)
            try:
                print "KEYBOARD: Write to Serial: %s" % cmd
                serial.write(cmd)
            except UnboundLocalError:
                sys.exit(0)


class SerialLineMonitorThread(threading.Thread):
    def __init__(self, serial):
        threading.Thread.__init__(self)
        self.serial = serial
        self.name = "serial_monitoring_thread"
        self.logger = logging.getLogger()

    def run(self):
        self.logger.debug("running")
        while True:
            try:
                print serial.readline(),
            except select.error as e:
                self.logger.debug("select.error - serial port no longer available. Exiting")
                sys.exit(0)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Remote Control Mock Up for AndroAtuin Robot.')
    parser.add_argument('-d', '--device', action='store', dest='device', required=True)
    parser.add_argument('-b', '--baud', action='store', type=int, dest='baud', required=True)
    parser.add_argument('-v', '--verbose', action='store_true', dest='verbose',)
    args = parser.parse_args()

    logger = logging.getLogger()
    logger.setLevel(logging.WARNING)
    if args.verbose:
        logger.setLevel(logging.DEBUG)
        logging.debug("Setting loglevel to DEBUG")


    # Open the serial Port
    serial = serial.Serial(baudrate=args.baud, port=args.device)

    #
    serial_monitor_thread = SerialLineMonitorThread(serial)
    keyboard_input_thread = InputThread(serial)

    serial_monitor_thread.start()
    keyboard_input_thread.start()
    logger.debug("serial_monitor_thread alive?: %s" % "YES" if serial_monitor_thread.is_alive else "NO")
    logger.debug("keyboard_input_thread alive?: %s" % "YES" if keyboard_input_thread.is_alive else "NO")
    keyboard_input_thread.join()
    logger.debug("keyboard_input_thread exited and joined")
    logger.debug("Will close serial port")
    serial.close()
    logger.debug("Serial port closed")
    serial_monitor_thread.join()
    logger.debug("serial_monitor_thread exited and joined")


