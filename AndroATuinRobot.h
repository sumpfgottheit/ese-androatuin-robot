// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef AndroATuinRobot_H_
#define AndroATuinRobot_H_
#include "Arduino.h"
//add your includes for the project AndroATuinRobot here
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

// Import Libraries necessary for URM 37 Ultraschall sensor
#include <SoftwareSerial.h>
#include "URMSerial.h"

//Debugging
//#define DEBUG 1
#include "debugutils.h"


//end of add your includes here
#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

//add your function definitions for the project AndroATuinRobot here




//Do not add code below this line
#endif /* AndroATuinRobot_H_ */
