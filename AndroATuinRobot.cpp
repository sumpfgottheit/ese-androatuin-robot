// Do not remove the include below
#include "AndroATuinRobot.h"

//Standard Speed DC control
const byte MotorLeftSpeedControlPin      = 5;
const byte MotorRightSpeedControlPin     = 6;
const byte MotorLeftDirectionControlPin  = 4;
const byte MotorRightDirectionControlPin = 7;

const byte FORWARD  = HIGH;
const byte BACKWARD = LOW;

// Definitions for LCD
const byte LCD_LINE_UPPER = 0;
const byte LCD_LINE_LOWER = 1;
const char LCD_CORNER_CLEAR_STRING[] = "        ";
char lcd_tmp_string[] = "        ";

struct lcd_cursorPos_t {
	byte line;
	byte row;
} lcd_cursorPos;

const lcd_cursorPos_t LCD_LEFT_UPPER = { 0, 0 };
const lcd_cursorPos_t LCD_LEFT_LOWER = { 0, 1 };
const lcd_cursorPos_t LCD_RIGHT_UPPER = { 8, 0 };
const lcd_cursorPos_t LCD_RIGHT_LOWER = { 8, 1 };

// Definitions for URM37 Ultraschall sensor
const byte URM37_MEASURE_DISTANCE	= 1;
const byte URM37_TX_PIN				= 2;
const byte URM37_RX_PIN				= 3;
const unsigned int URM37_BAUDRATE 	= 9600;
const char URM_DISTANCE_NA[]				= "cm  n/a ";
URMSerial urm;
int urmdistance			 			= 0;
int old_urmdistance 				= 0;



const unsigned long BAUDRATE = 9600;
const String sBAUDRATE = String(BAUDRATE);

const char DELIMITER = '|';

const byte MAX_COMMAND_LENGTH=30;

const byte RETURN_SUCCESS = 1;
const byte RETURN_FAILURE = 0;
const byte RETURN_COMMAND_STRING_NOT_YET_READY = 2;

byte  commandarrayIndex = 0;
char commandarray[MAX_COMMAND_LENGTH] ="\0";

String ERROR_STRING = String("");

byte return_append = 0;

struct motorCommand_t {
	byte directionLeft;
	byte speedLeft;
	byte directionRight;
	byte speedRight;
} motorcommand;

/**
 * Return a String for the left Motor, containing a '+' for foward
 * or a '-' for backward and the current speed
 */
String motorCommand_t_Left_to_String(motorCommand_t c) {
	char directionLeft;
	directionLeft = c.directionLeft == FORWARD ? '+' : '-';
	return "L:" + String(directionLeft) + c.speedLeft;
}

/**
 * Return a String for the right Motor, containing a '+' for foward
 * or a '-' for backward and the current speed
 */
String motorCommand_t_Right_to_String(motorCommand_t c) {
	char directionRight;
	directionRight = c.directionRight == FORWARD ? '+' : '-';
	return "R:" + String(directionRight) + c.speedRight;
}

/**
 * Return a String with human readable output of the given motorCommand_t
 */
String motorCommand_t_toString(motorCommand_t c) {
	return motorCommand_t_Left_to_String(c) + " / " + motorCommand_t_Right_to_String(c);
}

/**
 * Print the first 8 chars of the string on the given corner.
 * Clear it first
 */
void printLCDCorner(lcd_cursorPos_t cursor_pos, const char* s) {
	lcd.setCursor(cursor_pos.line, cursor_pos.row);
	lcd.print(LCD_CORNER_CLEAR_STRING);
	lcd.setCursor(cursor_pos.line, cursor_pos.row);
	byte l = strlen(s);
	byte i = 0;
	while (i<=8 && i<=l) {
		lcd.write(s[i]);
		i++;
	}
}

/**
 * Gibt den Motorcommand am LCD Display aus
 */
void lcd_printMotorCommand(motorCommand_t motorcommand){
	if (motorcommand.directionLeft) {
		sprintf(lcd_tmp_string, "L: +%03u ", motorcommand.speedLeft);
	} else {
		sprintf(lcd_tmp_string, "L: -%03u ", motorcommand.speedLeft);
	}
	printLCDCorner(LCD_LEFT_UPPER, lcd_tmp_string);

	if (motorcommand.directionRight) {
		sprintf(lcd_tmp_string, "R: +%03u ", motorcommand.speedRight);
	} else {
		sprintf(lcd_tmp_string, "R: -%03u ", motorcommand.speedRight);
	}
	printLCDCorner(LCD_RIGHT_UPPER, lcd_tmp_string);
}

/**
 * Execute the given motorcommand
 */
void executeMotorCommand(motorCommand_t c) {
	digitalWrite(MotorLeftDirectionControlPin, c.directionLeft);
	analogWrite(MotorLeftSpeedControlPin, c.speedLeft);
	digitalWrite(MotorRightDirectionControlPin, c.directionRight);
	analogWrite(MotorRightSpeedControlPin, c.speedRight);
}

/**
 * Read the commandarray, and set the struct with the right values.
 */
int decodeCommand2(char* a) {
	if (a[0] != 'E') {
		ERROR_STRING=("motorCommand does not start with 'E'");
		return RETURN_FAILURE;
	}
	motorcommand.directionLeft = byte(a[1]) ? FORWARD : BACKWARD;
	motorcommand.speedLeft = byte(a[2]);
	motorcommand.directionRight = byte(a[3]) ? FORWARD : BACKWARD;
	motorcommand.speedRight = byte(a[4]);

	debug_print("Will excute following Motorcommand:");
	debug_print("Motorcommand: " + motorCommand_t_toString(motorcommand));
	lcd_printMotorCommand( motorcommand );
	executeMotorCommand(motorcommand);
	return RETURN_SUCCESS;
}

void setup(void)
{
    // set the required pins to output
    byte i;
    for(i=4;i<=7;i++)
        pinMode(i, OUTPUT);
    Serial.begin(BAUDRATE);            //Set Baud Rate

    lcd.init();
    lcd.backlight();
    lcd.setCursor(0,0);
    lcd.print("-=*AndroATuin*=-");
    lcd.setCursor(0,1);
    lcd.print(String(BAUDRATE) + "bd");

    urm.begin(URM37_RX_PIN, URM37_TX_PIN, URM37_BAUDRATE);

    Serial.println("Welcome to AndroATuin App");
    debug_print("Debug funktioniert");
}

void loop(void)
{
	if (urm.requestMeasurementOrTimeout(URM37_MEASURE_DISTANCE, urmdistance) == URM37_MEASURE_DISTANCE) {

		if (urmdistance != old_urmdistance)  {
			if (sprintf(lcd_tmp_string, " cm %04u ", urmdistance)) {
				printLCDCorner(LCD_RIGHT_LOWER, lcd_tmp_string);
			}
			Serial.print(urmdistance);
			old_urmdistance = urmdistance;
		}

	} else {
		debug_print("urm has no reading availabile");
		printLCDCorner(LCD_RIGHT_LOWER, URM_DISTANCE_NA);
	}

    if(Serial.available()>=5){

    	// MotorcommandStrings sind 5 Zeichen lang -> holen
    	for (commandarrayIndex = 0; commandarrayIndex<5; commandarrayIndex++) {
    		commandarray[commandarrayIndex]= Serial.read();
    	}
    	commandarray[++commandarrayIndex] = '\0';

    	debug_print("Commandstring available");
    	debug_print(String(commandarray));

    	if (decodeCommand2(commandarray)) {
    		debug_print("commandstring successfully decoded");
    	} else {
    		debug_print("Failed to decode commandstring");
    		debug_print("Errorstring: " + ERROR_STRING);
    	}

    	for (commandarrayIndex = 0; commandarrayIndex <=5; commandarrayIndex++) {
    		commandarray[commandarrayIndex] = '\0';
    	}
    	commandarrayIndex=0;


    }
}

